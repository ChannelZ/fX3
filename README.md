# fX3

L0neStarr's fX3 engine for OpenArena updated and based on the latest ioquake3.  
  
### Overview of what was changed:
- the engine can make walls and floors flat(untextured) same as using r_lightmap except you can change the color
- maps can be made darker similar to r_mapOverbrightBits except you have more control over it
- announcer sound volume can be changed
- tabs in the console
- a clock in the console
- console filters(disabled at compile-time)
- console background and font can be changed
- more console customizations(originally from Tremulous I think, imported it from OpenArena)
- raw mouse input on GNU/Linux and FreeBSD using libevdev, Windows needs some more work
- support for a few more operating systems
- random GUID can be enabled
- [QWFWD](https://www.quakeworld.nu/wiki/QWfwd) proxy support, [list of servers](https://www.quakeservers.net//quakeworld/servers/t=proxy/)
- network statistics from ioq3_urt_mitsu was imported
- a few enhancements for Un*x-like systems
- default shader can be replaced
- color of the sky can be changed
  
### Why?  
Because I play other Quake games and these have clients which have most of the features above and I missed them in ioquake3.  

### Credits:
- id Software for the game
- ioq3 team for the ton of updates
- LoneStarr for some new features
- ^bubu for raw mouse input from mq3(defunct)
- Mitsu(I think that's his name) for some features from ioq3_urt_mitsu
- OpenArena for some changes on the console, I think they actually come from Tremulous

### New CVars:  
| CVar | Description |
|------|-------------|
|cl_consoleType                      | switches between stock background or solid background color|
|cl_consoleColorRed                  | amount of red in the solid background color|
|cl_consoleColorGreen                | amount of green in the solid background color|
|cl_consoleColorBlue                 | amount of blue in the solid background color|
|cl_consoleHeight                    | console height, 1.0 is full height|
|cl_conYOffset                       | Y offset for the notification lines|
|con_notifylines                     | number of notification lines to display|
|cl_ownchatcolor                     | colors your chat and team chat messages, needs 2 values in the string, for example "2 5"|
|conback                             | enables a custom console background image|
|conchar                             | enables a custom console charset|
|concharFile                         | specifies which file to use for the charset|
|con_toggleKey                       | EXPERIMENTAL, disables the use of ~ as console key, for using it for something else maybe|
|con_clocktype                       | switches between 24Hour clock and AM/PM clock|
|con_filters0 to con_filters39       | regexp filters for the console, every match will not be displayed|
|con_filter                          | enables the console filters|
|cl_randomguid                       | enables GUID randomization|
|fwd_use                             | enables the QWFWD proxy|
|fwd_addr                            | address of a QWFWD proxy|
|cl_drawping                         | enables ping statistics display|
|cl_drawpingfontsize                 | font size ping statistics display|
|cl_drawpingposx                     | X position of the ping statistics display|
|cl_drawpingposy                     | Y position of the ping statistics display|
|cl_drawpingfirstinterval            | interval before calculating ping statistics|
|cl_drawpingsecondinterval           | same as the first ping interval, it's just being repeated|
|cl_drawsnaps                        | enables snaps statistics display|
|cl_drawsnapsfontsize                | font size snaps statistics display|
|cl_drawsnapsposx                    | X position of the snaps statistics display|
|cl_drawsnapsposy                    | Y position of the snaps statistics display|
|cl_drawsnapsfirstinterval           | interval before calculating snaps statistics|
|cl_drawsnapssecondinterval          | same as the first snaps interval, it's just being repeated|
|cl_drawpackets                      | enables snaps statistics display|
|cl_drawpacketsfontsize              | font size packets statistics display|
|cl_drawpacketsposx                  | X position of the packets statistics display|
|cl_drawpacketsposy                  | Y position of the packets statistics display|
|cl_drawpacketsfirstinterval         | interval before calculating packets statistics|
|cl_drawpacketssecondinterval        | same as the first packets interval, it's just being repeated|
|s_announcerVolume                   | controls the volume for the announcer voice|
|in_rawdevice                        | to set the mouse device used for raw reading (GNU/Linux and FreeBSD only)|
|in_autorawdevice                    | sets in_rawdevice automatically to the first valid device found|
|r_defShader                         | sets the texture to use instead of the default shader|
|r_fastskycolor                      | sets the color of the sky, needs 3 float values, for example "0 0 .3"|
|r_drawFlat                          | enables flat(untextured) walls/floors|
|r_drawFlatRed                       | amount of red in percent for r_drawFlat|
|r_drawFlatGreen                     | amount of green in percent for r_drawFlat|
|r_drawFlatBlue                      | amount of blue in percent for r_drawFlat|
|r_darkness                          | amount of darkness of the map in percent(doesn't work with r_drawFlat enabled)|


### New commands:
| Command | Description |
|---------|-------------|
|in_listmice                         | lists valid mice to use for raw input|
|in_mouseinfo                        | outputs informations about the used raw input device|
|reguid                              | generates a new GUID if cl_randomguid is enabled|
|fwdinfo                             | shows infos about the currently used QWFWD proxy|


### Compilation and installation

For *nix
  1. Change to the directory containing this readme.
  2. Run 'make'.

For Windows,
  1. Please refer to the excellent instructions here:
     https://ioquake3.org/help/building-ioquake3/

For macOS, building a Universal Binary (x86_64, x86, ppc)
  1. Install MacOSX SDK packages from XCode.  For maximum compatibility,
     install MacOSX10.5.sdk and MacOSX10.6.sdk.
  2. Change to the directory containing this README file.
  3. Run './make-macosx-ub.sh'
  4. Copy the resulting ioquake3.app in /build/release-darwin-universal
     to your /Applications/ioquake3 folder.

For Mac OS X, building a Universal Binary 2 (arm64, x86_64)
  1. Install MacOSX SDK packages from XCode.  Building for arm64 requires
     MacOSX11.sdk or later.
  2. Change to the directory containing this README file.
  3. Run './make-macosx-ub2.sh'
  4. Copy the resulting ioquake3.app in /build/release-darwin-universal2
     to your /Applications/ioquake3 folder.

Installation, for *nix
  1. Set the COPYDIR variable in the shell to be where you installed Quake 3
     to. By default it will be /usr/local/games/quake3 if you haven't set it.
     This is the path as used by the original Linux Q3 installer and subsequent
     point releases.
  2. Run 'make copyfiles'.

It is also possible to cross compile for Windows under *nix using MinGW. Your
distribution may have mingw32 packages available. On debian/Ubuntu, you need to
install 'mingw-w64'. Thereafter cross compiling is simply a case running
'PLATFORM=mingw32 ARCH=x86 make' in place of 'make'. ARCH may also be set to
x86_64.

The following variables may be set, either on the command line or in
Makefile.local:

```
  CFLAGS               - use this for custom CFLAGS
  V                    - set to show cc command line when building
  DEFAULT_BASEDIR      - extra path to search for baseq3 and such
  BUILD_SERVER         - build the 'ioq3ded' server binary
  BUILD_CLIENT         - build the 'ioquake3' client binary
  BUILD_BASEGAME       - build the 'baseq3' binaries
  BUILD_MISSIONPACK    - build the 'missionpack' binaries
  BUILD_GAME_SO        - build the game shared libraries
  BUILD_GAME_QVM       - build the game qvms
  BUILD_STANDALONE     - build binaries suited for stand-alone games
  SERVERBIN            - rename 'ioq3ded' server binary
  CLIENTBIN            - rename 'ioquake3' client binary
  USE_RENDERER_DLOPEN  - build and use the renderer in a library
  USE_YACC             - use yacc to update code/tools/lcc/lburg/gram.c
  BASEGAME             - rename 'baseq3'
  BASEGAME_CFLAGS      - custom CFLAGS for basegame
  MISSIONPACK          - rename 'missionpack'
  MISSIONPACK_CFLAGS   - custom CFLAGS for missionpack (default '-DMISSIONPACK')
  USE_OPENAL           - use OpenAL where available
  USE_OPENAL_DLOPEN    - link with OpenAL at runtime
  USE_CURL             - use libcurl for http/ftp download support
  USE_CURL_DLOPEN      - link with libcurl at runtime
  USE_CODEC_VORBIS     - enable Ogg Vorbis support
  USE_CODEC_OPUS       - enable Ogg Opus support
  USE_MUMBLE           - enable Mumble support
  USE_VOIP             - enable built-in VoIP support
  USE_FREETYPE         - enable FreeType support for rendering fonts
  USE_INTERNAL_LIBS    - build internal libraries instead of dynamically
                         linking against system libraries; this just sets
                         the default for USE_INTERNAL_ZLIB etc.
                         and USE_LOCAL_HEADERS
  USE_INTERNAL_ZLIB    - build and link against internal zlib
  USE_INTERNAL_JPEG    - build and link against internal JPEG library
  USE_INTERNAL_OGG     - build and link against internal ogg library
  USE_INTERNAL_OPUS    - build and link against internal opus/opusfile libraries
  USE_LOCAL_HEADERS    - use headers local to ioq3 instead of system ones
  DEBUG_CFLAGS         - C compiler flags to use for building debug version
  COPYDIR              - the target installation directory
  TEMPDIR              - specify user defined directory for temp files
```

The defaults for these variables differ depending on the target platform.
