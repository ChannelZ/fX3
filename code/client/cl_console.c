/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/
// console.c

#include "client.h"

#ifdef CONFILTER
#	define PCRE_STATIC 1
#	include <pcre.h>
#endif

int g_console_field_width = 78;

#ifdef FX3
#	define NUM_CON_TIMES 40

#	ifdef CONSOLETESTING
#		define MAX_CONSOLES 68
#	else
#		define MAX_CONSOLES 5
#	endif
#else
#define	NUM_CON_TIMES 4
#endif

#define		CON_TEXTSIZE	32768
typedef struct {
	qboolean	initialized;

	short	text[CON_TEXTSIZE];
	int		current;		// line where next message will be printed
	int		x;				// offset in current line for next print
	int		display;		// bottom of console displays this line

	int 	linewidth;		// characters across screen
	int		totallines;		// total lines in console scrollback

	float	xadjust;		// for wide aspect screens

	float	displayFrac;	// aproaches finalFrac at scr_conspeed
	float	finalFrac;		// 0.0 to 1.0 lines of console to display

	int		vislines;		// in scanlines

	int		times[NUM_CON_TIMES];	// cls.realtime time the line was generated
								// for transparent notify lines
	vec4_t	color;

#ifdef FX3
	float		userFrac;
	char		*name;
	qboolean	notify;
	qboolean	active;
#endif
} console_t;

#ifdef FX3
#	define CON_ALL  0
#	define CON_SYS  1
#	define CON_CHT  2
#	define CON_TCHT 3 
#	ifndef CONSOLETESTING
#		define CON_TELL 4
#	endif
#	define CON_PCHT	5
#	define DEFAULT_CON CON_ALL

char *conName[] = {
	"All",
	"Sys",
	"Chat",
	"Team",
	"Tell",
	"Priv"
};

#	ifndef CONSOLETESTING
int numConsoles = MAX_CONSOLES;
#	else
int numConsoles = 4;
#	endif 
int activeConsoleNum = DEFAULT_CON;
int lastSrvCmdNum = -1;
//qboolean isChat;

console_t	con[MAX_CONSOLES]; //  MAX_CONSOLES is higher than numConsoles because I may need more than 5 later
console_t	*activeCon = &con[DEFAULT_CON];
#else
console_t	con;
#endif

cvar_t		*con_conspeed;
cvar_t		*con_autoclear;
cvar_t		*con_notifytime;

#ifdef FX3
cvar_t		*con_timestamp;
cvar_t		*con_clocktype;
cvar_t		*con_width;
#	ifdef CONFILTER
cvar_t		*con_filters[MAX_CON_FILTERS];
cvar_t		*con_filter;
pcre		*con_filters_compiled[MAX_CON_FILTERS];
pcre		*con_timestampre;	
#	endif
#endif

#define	DEFAULT_CONSOLE_WIDTH	78


/*
================
Con_ToggleConsole_f
================
*/
void Con_ToggleConsole_f (void) {
	// Can't toggle the console when it's the only thing available
	if ( clc.state == CA_DISCONNECTED && Key_GetCatcher( ) == KEYCATCH_CONSOLE ) {
		return;
	}

	if ( con_autoclear->integer ) {
		Field_Clear( &g_consoleField );
	}

#ifdef FX3
	if(con_width->integer)
		g_consoleField.widthInChars = con_width->integer;
	else
#endif
	g_consoleField.widthInChars = g_console_field_width;

	Con_ClearNotify ();
	Key_SetCatcher( Key_GetCatcher( ) ^ KEYCATCH_CONSOLE );
}

/*
===================
Con_ToggleMenu_f
===================
*/
void Con_ToggleMenu_f( void ) {
	CL_KeyEvent( K_ESCAPE, qtrue, Sys_Milliseconds() );
	CL_KeyEvent( K_ESCAPE, qfalse, Sys_Milliseconds() );
}

/*
================
Con_MessageMode_f
================
*/
void Con_MessageMode_f (void) {
	chat_playerNum = -1;
	chat_team = qfalse;
	Field_Clear( &chatField );
	chatField.widthInChars = 30;

	Key_SetCatcher( Key_GetCatcher( ) ^ KEYCATCH_MESSAGE );
}

/*
================
Con_MessageMode2_f
================
*/
void Con_MessageMode2_f (void) {
	chat_playerNum = -1;
	chat_team = qtrue;
	Field_Clear( &chatField );
	chatField.widthInChars = 25;
	Key_SetCatcher( Key_GetCatcher( ) ^ KEYCATCH_MESSAGE );
}

/*
================
Con_MessageMode3_f
================
*/
void Con_MessageMode3_f (void) {
	chat_playerNum = VM_Call( cgvm, CG_CROSSHAIR_PLAYER );
	if ( chat_playerNum < 0 || chat_playerNum >= MAX_CLIENTS ) {
		chat_playerNum = -1;
		return;
	}
	chat_team = qfalse;
	Field_Clear( &chatField );
	chatField.widthInChars = 30;
	Key_SetCatcher( Key_GetCatcher( ) ^ KEYCATCH_MESSAGE );
}

/*
================
Con_MessageMode4_f
================
*/
void Con_MessageMode4_f (void) {
	chat_playerNum = VM_Call( cgvm, CG_LAST_ATTACKER );
	if ( chat_playerNum < 0 || chat_playerNum >= MAX_CLIENTS ) {
		chat_playerNum = -1;
		return;
	}
	chat_team = qfalse;
	Field_Clear( &chatField );
	chatField.widthInChars = 30;
	Key_SetCatcher( Key_GetCatcher( ) ^ KEYCATCH_MESSAGE );
}

/*
================
Con_Clear_f
================
*/
void Con_Clear_f (void) {
	int		i;

	for ( i = 0 ; i < CON_TEXTSIZE ; i++ ) {
#ifdef FX3
		activeCon->text[i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#else
		con.text[i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#endif
	}

	Con_Bottom();		// go to end
}

						
/*
================
Con_Dump_f

Save the console contents out to a file
================
*/
void Con_Dump_f (void)
{
	int		l, x, i;
	short	*line;
	fileHandle_t	f;
	int		bufferlen;
	char	*buffer;
	char	filename[MAX_QPATH];

	if (Cmd_Argc() != 2)
	{
		Com_Printf ("usage: condump <filename>\n");
		return;
	}

	Q_strncpyz( filename, Cmd_Argv( 1 ), sizeof( filename ) );
	COM_DefaultExtension( filename, sizeof( filename ), ".txt" );

	if (!COM_CompareExtension(filename, ".txt"))
	{
		Com_Printf("Con_Dump_f: Only the \".txt\" extension is supported by this command!\n");
		return;
	}

	f = FS_FOpenFileWrite( filename );
	if (!f)
	{
		Com_Printf ("ERROR: couldn't open %s.\n", filename);
		return;
	}

	Com_Printf ("Dumped console text to %s.\n", filename );

	// skip empty lines
#ifdef FX3
	for (l = activeCon->current - activeCon->totallines + 1 ; l <= activeCon->current ; l++)
#else
	for (l = con.current - con.totallines + 1 ; l <= con.current ; l++)
#endif
	{
#ifdef FX3
		line = activeCon->text + (l%activeCon->totallines)*activeCon->linewidth;
		for (x=0 ; x < activeCon->linewidth ; x++)
#else
		line = con.text + (l%con.totallines)*con.linewidth;
		for (x=0 ; x<con.linewidth ; x++)
#endif
			if ((line[x] & 0xff) != ' ')
				break;
#ifdef FX3
		if (x != activeCon->linewidth)
#else
		if (x != con.linewidth)
#endif
			break;
	}

#ifdef _WIN32
#	ifdef FX3
	bufferlen = activeCon->linewidth + 3 * sizeof ( char );
#	else
	bufferlen = con.linewidth + 3 * sizeof ( char );
#	endif
#else
#	ifdef FX3
	bufferlen = activeCon->linewidth + 2 * sizeof ( char );
#	else
	bufferlen = con.linewidth + 2 * sizeof ( char );
#	endif
#endif

	buffer = Hunk_AllocateTempMemory( bufferlen );

	// write the remaining lines
	buffer[bufferlen-1] = 0;
#ifdef FX3
	for ( ; l <= activeCon->current ; l++)
	{

		line = activeCon->text + (l%activeCon->totallines)*activeCon->linewidth;
		for(i=0; i < activeCon->linewidth; i++)
#else
	for ( ; l <= con.current ; l++)
	{

		line = con.text + (l%con.totallines)*con.linewidth;
		for(i=0; i<con.linewidth; i++)
#endif
			buffer[i] = line[i] & 0xff;
#ifdef FX3
		for (x=activeCon->linewidth-1 ; x>=0 ; x--)
#else
		for (x=con.linewidth-1 ; x>=0 ; x--)
#endif
		{
			if (buffer[x] == ' ')
				buffer[x] = 0;
			else
				break;
		}
#ifdef _WIN32
		Q_strcat(buffer, bufferlen, "\r\n");
#else
		Q_strcat(buffer, bufferlen, "\n");
#endif
		FS_Write(buffer, strlen(buffer), f);
	}

	Hunk_FreeTempMemory( buffer );
	FS_FCloseFile( f );
}

						
/*
================
Con_ClearNotify
================
*/
void Con_ClearNotify( void ) {
	int		i;
	
	for ( i = 0 ; i < NUM_CON_TIMES ; i++ ) {
#ifdef FX3
		con[CON_ALL].times[i] = 0;
#else
		con.times[i] = 0;
#endif
	}
}

						

/*
================
Con_CheckResize

If the line width has changed, reformat the buffer.
================
*/
#ifdef FX3
void Con_CheckResize (console_t *con)
#else
void Con_CheckResize (void)
#endif
{
	int		i, j, width, oldwidth, oldtotallines, numlines, numchars;
	short	tbuf[CON_TEXTSIZE];

	width = (SCREEN_WIDTH / SMALLCHAR_WIDTH) - 2;

#ifdef FX3
	if (width == con->linewidth)
#else
	if (width == con.linewidth)
#endif
		return;

	if (width < 1)			// video hasn't been initialized yet
	{
#ifdef FX3
		if(con_width->integer)
			width = con_width->integer;
		else
#endif
		width = DEFAULT_CONSOLE_WIDTH;
#ifdef FX3
		con->linewidth = width;
		con->totallines = CON_TEXTSIZE / activeCon->linewidth;
#else
		con.linewidth = width;
		con.totallines = CON_TEXTSIZE / con.linewidth;
#endif
		for(i=0; i<CON_TEXTSIZE; i++)

#ifdef FX3
			con->text[i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#else
			con.text[i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#endif
	}
	else
	{
#ifdef FX3
		oldwidth = con->linewidth;
		con->linewidth = width;
		oldtotallines = con->totallines;
		con->totallines = CON_TEXTSIZE / con->linewidth;
#else
		oldwidth = con.linewidth;
		con.linewidth = width;
		oldtotallines = con.totallines;
		con.totallines = CON_TEXTSIZE / con.linewidth;
#endif
		numlines = oldtotallines;

#ifdef FX3
		if (con->totallines < numlines)
			numlines = con->totallines;
#else
		if (con.totallines < numlines)
			numlines = con.totallines;
#endif

		numchars = oldwidth;

#ifdef FX3
		if (con->linewidth < numchars)
			numchars = con->linewidth;
#else
		if (con.linewidth < numchars)
			numchars = con.linewidth;
#endif

#ifdef FX3
		Com_Memcpy (tbuf, con->text, CON_TEXTSIZE * sizeof(short));
#else
		Com_Memcpy (tbuf, con.text, CON_TEXTSIZE * sizeof(short));
#endif
		for(i=0; i<CON_TEXTSIZE; i++)

#ifdef FX3
			con->text[i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#else
			con.text[i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#endif


		for (i=0 ; i<numlines ; i++)
		{
			for (j=0 ; j<numchars ; j++)
			{
#ifdef FX3
				con->text[(con->totallines - 1 - i) * con->linewidth + j] = tbuf[((con->current - i + oldtotallines) % oldtotallines) * oldwidth + j];
#else
				con.text[(con.totallines - 1 - i) * con.linewidth + j] =
						tbuf[((con.current - i + oldtotallines) %
							  oldtotallines) * oldwidth + j];
#endif
			}
		}

		Con_ClearNotify ();
	}

#ifdef FX3
	con->current = con->totallines - 1;
	con->display = con->current;
#else
	con.current = con.totallines - 1;
	con.display = con.current;
#endif
}

#ifdef FX3
void Con_PrevTab(void){
	int i = MAX_CONSOLES - 1;

	activeConsoleNum--;
	if(activeConsoleNum < CON_ALL){// wrap around...
		while(!con[i].active){
			i--;
		}
		activeConsoleNum = i;
	}

	activeCon = &con[activeConsoleNum];
	activeCon->notify = qfalse;
}

void Con_NextTab(void){
	int i = 0;

	activeConsoleNum++;
	if(!con[activeConsoleNum].active){
		while(!con[i].active){
		i++;
			if(i >= MAX_CONSOLES){
				i = 0;
			}
		}
		activeConsoleNum = i;
	}

	activeCon = &con[activeConsoleNum];
	activeCon->notify = qfalse;
}

void Con_SwitchToTab(int i){
	if(i >= MAX_CONSOLES || i < 0){
		return;
	}

        activeConsoleNum = i;
        activeCon = &con[activeConsoleNum];
        activeCon->notify = qfalse;
}
#endif

/*
==================
Cmd_CompleteTxtName
==================
*/
void Cmd_CompleteTxtName( char *args, int argNum ) {
	if( argNum == 2 ) {
		Field_CompleteFilename( "", "txt", qfalse, qtrue );
	}
}


/*
================
Con_Init
================
*/
void Con_Init (void) {
	int		i;

#ifdef FX3
	con_timestamp = Cvar_Get ("con_timestamp", "0", 0);
	con_notifytime = Cvar_Get ("con_notifyperiod", "3", 0);
	con_width = Cvar_Get ("con_width", "", 0);
#else
	con_notifytime = Cvar_Get ("con_notifytime", "3", 0);
#endif
	con_conspeed = Cvar_Get ("scr_conspeed", "3", 0);
	con_autoclear = Cvar_Get("con_autoclear", "1", CVAR_ARCHIVE);

	Field_Clear( &g_consoleField );
#ifdef FX3
	if(con_width->integer)
		g_consoleField.widthInChars = con_width->integer;
	else
#endif
	g_consoleField.widthInChars = g_console_field_width;
	for ( i = 0 ; i < COMMAND_HISTORY ; i++ ) {
		Field_Clear( &historyEditLines[i] );
#ifdef FX3
		if(con_width->integer)
			historyEditLines[i].widthInChars = con_width->integer;
		else
#endif
		historyEditLines[i].widthInChars = g_console_field_width;
	}
#ifdef CONFILTER
	con_filter = Cvar_Get("con_filter", "1", CVAR_ARCHIVE);
	for (i=0; i<MAX_CON_FILTERS; i++) {
		con_filters[i] = Cvar_Get(va("con_filter%i", i), "", CVAR_ARCHIVE);
	}
#endif
#ifdef FX3
	con_clocktype = Cvar_Get("con_clocktype", "0", CVAR_ARCHIVE);
#endif
	CL_LoadConsoleHistory( );

	Cmd_AddCommand ("toggleconsole", Con_ToggleConsole_f);
	Cmd_AddCommand ("togglemenu", Con_ToggleMenu_f);
	Cmd_AddCommand ("messagemode", Con_MessageMode_f);
	Cmd_AddCommand ("messagemode2", Con_MessageMode2_f);
	Cmd_AddCommand ("messagemode3", Con_MessageMode3_f);
	Cmd_AddCommand ("messagemode4", Con_MessageMode4_f);
	Cmd_AddCommand ("clear", Con_Clear_f);
	Cmd_AddCommand ("condump", Con_Dump_f);
	Cmd_SetCommandCompletionFunc( "condump", Cmd_CompleteTxtName );
}

/*
================
Con_Shutdown
================
*/
void Con_Shutdown(void)
{
	Cmd_RemoveCommand("toggleconsole");
	Cmd_RemoveCommand("togglemenu");
	Cmd_RemoveCommand("messagemode");
	Cmd_RemoveCommand("messagemode2");
	Cmd_RemoveCommand("messagemode3");
	Cmd_RemoveCommand("messagemode4");
	Cmd_RemoveCommand("clear");
	Cmd_RemoveCommand("condump");
}

/*
===============
Con_Linefeed
===============
*/
#ifdef FX3
void Con_Linefeed (console_t *con, qboolean skipnotify)
#else
void Con_Linefeed (qboolean skipnotify)
#endif
{
	int		i;

#ifdef CONFILTER
	//  this is stuff for the console filters, from ioq3-urt IIRC

	char txt[MAXPRINTMSG];
	char *txtt=txt;
	pcre **re;
	int ovector[30];
	char ch;
	qboolean copy;

	// Cgg Filters
	if (con->linewidth < sizeof(txt) && Cvar_VariableIntegerValue("con_filter")) {
		for (i=con->linewidth-1,copy=qfalse; i>=0; i--) {
			ch = con->text[(con->current%con->totallines)*con->linewidth+i] &0xff;
			if (ch != ' ') {
				copy = qtrue;
			}
			txt[i] = (copy) ? ch : 0;
		}

	if (con_timestamp && con_timestamp->integer && pcre_exec(con_timestampre, NULL, txt, strlen(txt), 0, 0, ovector, 30) > 0){
		txtt += 9;
	}

		for (re=con_filters_compiled; re-con_filters_compiled<MAX_CON_FILTERS; re++) {
			if (*re && pcre_exec(*re, NULL, txtt, strlen(txtt), 0, 0, ovector, 30) > 0) {
				con->x = 0;
				for(i=0; i<con->linewidth; i++) {
					con->text[(con->current%con->totallines)*con->linewidth+i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
				}
				return;
			}
		}
	}
	//Cgg End

#endif

	// mark time for transparent overlay
#ifdef FX3
	if (con->current >= 0){
		if (skipnotify)
			con->times[con->current % NUM_CON_TIMES] = 0;
		else
			con->times[con->current % NUM_CON_TIMES] = cls.realtime;
	}

	con->x = 0;
	if (con->display == con->current)
		con->display++;
	con->current++;
	for(i=0; i<con->linewidth; i++)
		con->text[(con->current%con->totallines)*con->linewidth+i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#else
	if (con.current >= 0)
	{

		if (skipnotify)
			con.times[con.current % NUM_CON_TIMES] = 0;
		else
			con.times[con.current % NUM_CON_TIMES] = cls.realtime;
	}

	con.x = 0;
	if (con.display == con.current)
		con.display++;
	con.current++;
	for(i=0; i<con.linewidth; i++)
		con.text[(con.current%con.totallines)*con.linewidth+i] = (ColorIndex(COLOR_WHITE)<<8) | ' ';
#endif
}

#ifdef FX3
int cmdchk(char str1[], char str2[]){
	int i, j;

	if(strlen(str1) >= strlen(str2)){

		j = strlen(str2);

		for (i = 0; i < j; i++){
			if(str1[i] != str2[i]){
				//Com_Printf("didn't match:\nstr1: %s val: %d pos: %d\nstr2: %s val: %d\n", str1, str1[i], i, str2, str2[i]);
				return 1;
			}
		}
	} else {
		 return 1;
	}

	return 0;
}

#define Q_RAW_ESCAPE 1
void CL_OutputToConsole(console_t *con, char *txt, qboolean skipnotify){
	int y, l, prev;
	unsigned char c;
	unsigned short color;
	char raw;

	if (con->x==0 && con_timestamp && con_timestamp->integer) {
                char txtt[MAXPRINTMSG];
                qtime_t now;
                Com_RealTime( &now );
                Com_sprintf(txtt,sizeof(txtt),"^9%02d:%02d:%02d ^7%s",now.tm_hour,now.tm_min,now.tm_sec,txt);
                strcpy(txt,txtt);
        }

	color = ColorIndex(COLOR_WHITE);

	while ( (c = *((unsigned char *) txt)) != 0 ) {
                if ( Q_IsColorString( txt ) ) {
                        color = ColorIndex( *(txt+1) );
                        txt += 2;
                        continue;
                }

		// count word length
                for (l=0 ; l< con->linewidth ; l++) {
                        if ( txt[l] <= ' ') {
                                break;
                        }

                }

                // word wrap
                if (l != con->linewidth && (con->x + l >= con->linewidth) ) {
                        Con_Linefeed(con, skipnotify);
                }

		txt++;

		switch (c){
			case Q_RAW_ESCAPE:
				raw = raw^1;
				break;
			case '\n':
				Con_Linefeed (con, skipnotify);
				break;
			case '\r':
				con->x = 0;
				break;
			default:        // display character and advance
				y = con->current % con->totallines;
				con->text[y*con->linewidth+con->x] = (color << 8) | c;
				con->x++;
				if(con->x >= con->linewidth)
					Con_Linefeed(con, skipnotify);
				break;
                }
        }

	// mark time for transparent overlay
	if (con->current >= 0) {
		// NERVE - SMF
		if ( skipnotify ) {
			prev = con->current % NUM_CON_TIMES - 1;
			if ( prev < 0 )
				prev = NUM_CON_TIMES - 1;
			con->times[prev] = 0;
		} else {
		// -NERVE - SMF
			con->times[con->current % NUM_CON_TIMES] = cls.realtime;
		}
	}
}

void CL_TellPlayerName(char *string, char *name){
	int             i, j;
	qboolean        skip = qtrue;
	//char          name[16];

	j = 0;

	for(i = 0;i < strlen(string);i++){
		if(string[i] == '['){
			skip = qfalse;
			j = 0;
			i++;
		}

		if(!skip){
			name[j] = string[i];
			j++;
		}

		//j++;

		if((string[i+4] == ':' && string[i+5] == ' ') || j == 15){ //'\x19'){
			name[j] = '\0';
			break;
		}
	}
}

void CL_ConsolePrint( char *txt ) {
        qboolean skipnotify = qfalse;           // NERVE - SMF

        int i, j;
	int serverCommandNumber;
	char *cmdstring;

        // TTimo - prefix for text that shows up in console but not in notify
        // backported from RTCW
        if ( !Q_strncmp( txt, "[skipnotify]", 12 ) ) {
                skipnotify = qtrue;
                txt += 12;
        }

        // for some demos we don't want to ever show anything on the console
        if ( cl_noprint && cl_noprint->integer ) {
                return;
	}


        //  initialize all the consoles!
	for(i=0; i < MAX_CONSOLES; i++){
		if (!con[i].initialized) {
			con[i].color[0] =
			con[i].color[1] =
			con[i].color[2] =
			con[i].color[3] = 1.0f;
			con[i].linewidth = -1;
			con[i].name = conName[i];
			Con_CheckResize(&con[i]);
			con[i].initialized = qtrue;

			if(i < numConsoles){
				con[i].active = qtrue;
				con[i].name = conName[i];
			} else {
				con[i].active = qfalse;
				con[i].name = '\0';
			}

			con[i].notify = qfalse;
		}
	}

	serverCommandNumber = clc.lastExecutedServerCommand;

	cmdstring = clc.serverCommands[ serverCommandNumber & ( MAX_RELIABLE_COMMANDS - 1 ) ];

	j = CON_SYS;

	if(serverCommandNumber > lastSrvCmdNum){ //  hopefully skips garbage this way...
		if(!cmdchk(cmdstring, "chat")) //Normal chat
			j = CON_CHT;

		if(!cmdchk(cmdstring, "chat \"\x19") || !cmdchk(cmdstring, "tell")){ //This is a /tell chat
#ifndef CONSOLETESTING
			j = CON_TELL;
			con[j].name = conName[j];
#else
			j = numConsoles;
			con[j].name = CL_TellPlayerName(txt);
#endif
			if(!con[j].active)
				con[j].active = qtrue;
		}

		if(!cmdchk(cmdstring, "tchat")) //Team chat
			j = CON_TCHT;
	}

	lastSrvCmdNum = serverCommandNumber;

	if(j >= CON_CHT){
		//isChat = qtrue;
		if(activeCon != &con[j]){
			con[j].notify = qtrue; //  hilights the console if qtrue
		}
	//} else {
		//isChat = qfalse;
	}

	//  send the stuff to the selected console con[j] ...

	CL_OutputToConsole(&con[j], txt, skipnotify);
	CL_OutputToConsole(&con[CON_ALL], txt, skipnotify);
}
#else
/*
================
CL_ConsolePrint

Handles cursor positioning, line wrapping, etc
All console printing must go through this in order to be logged to disk
If no console is visible, the text will appear at the top of the game window
================
*/
void CL_ConsolePrint( char *txt ) {
	int		y, l;
	unsigned char	c;
	unsigned short	color;
	qboolean skipnotify = qfalse;		// NERVE - SMF
	int prev;							// NERVE - SMF

	// TTimo - prefix for text that shows up in console but not in notify
	// backported from RTCW
	if ( !Q_strncmp( txt, "[skipnotify]", 12 ) ) {
		skipnotify = qtrue;
		txt += 12;
	}
	
	// for some demos we don't want to ever show anything on the console
	if ( cl_noprint && cl_noprint->integer ) {
		return;
	}
	
	if (!con.initialized) {
		con.color[0] = 
		con.color[1] = 
		con.color[2] =
		con.color[3] = 1.0f;
		con.linewidth = -1;
		Con_CheckResize ();
		con.initialized = qtrue;
	}

	color = ColorIndex(COLOR_WHITE);

	while ( (c = *((unsigned char *) txt)) != 0 ) {
		if ( Q_IsColorString( txt ) ) {
			color = ColorIndex( *(txt+1) );
			txt += 2;
			continue;
		}

		// count word length
		for (l=0 ; l< con.linewidth ; l++) {
			if ( txt[l] <= ' ') {
				break;
			}

		}

		// word wrap
		if (l != con.linewidth && (con.x + l >= con.linewidth) ) {
			Con_Linefeed(skipnotify);

		}

		txt++;

		switch (c)
		{
		case '\n':
			Con_Linefeed (skipnotify);
			break;
		case '\r':
			con.x = 0;
			break;
		default:	// display character and advance
			y = con.current % con.totallines;
			con.text[y*con.linewidth+con.x] = (color << 8) | c;
			con.x++;
			if(con.x >= con.linewidth)
				Con_Linefeed(skipnotify);
			break;
		}
	}


	// mark time for transparent overlay
	if (con.current >= 0) {
		// NERVE - SMF
		if ( skipnotify ) {
			prev = con.current % NUM_CON_TIMES - 1;
			if ( prev < 0 )
				prev = NUM_CON_TIMES - 1;
			con.times[prev] = 0;
		}
		else
		// -NERVE - SMF
			con.times[con.current % NUM_CON_TIMES] = cls.realtime;
	}
}
#endif

/*
==============================================================================

DRAWING

==============================================================================
*/


/*
================
Con_DrawInput

Draw the editline after a ] prompt
In Shake it's a > prompt
================
*/
void Con_DrawInput (void) {
	int		y;

	if ( clc.state != CA_DISCONNECTED && !(Key_GetCatcher( ) & KEYCATCH_CONSOLE ) ) {
		return;
	}

#ifdef FX3
	y = activeCon->vislines - ( SMALLCHAR_HEIGHT * 2 );

	re.SetColor( activeCon->color );

	SCR_DrawSmallChar( activeCon->xadjust + 1 * SMALLCHAR_WIDTH, y, '>' );

	Field_Draw( &g_consoleField, activeCon->xadjust + 2 * SMALLCHAR_WIDTH, y, SCREEN_WIDTH - 3 * SMALLCHAR_WIDTH, qtrue, qtrue );
#else
	y = con.vislines - ( SMALLCHAR_HEIGHT * 2 );

	re.SetColor( con.color );

	SCR_DrawSmallChar( con.xadjust + 1 * SMALLCHAR_WIDTH, y, ']' );

	Field_Draw( &g_consoleField, con.xadjust + 2 * SMALLCHAR_WIDTH, y,
		SCREEN_WIDTH - 3 * SMALLCHAR_WIDTH, qtrue, qtrue );
#endif
}


/*
================
Con_DrawNotify

Draws the last few lines of output transparently over the game top
================
*/
void Con_DrawNotify (void)
{
	int		x, v;
	short	*text;
	int		i;
	int		time;
	int		skip;
	int		currentColor;

	currentColor = 7;
	re.SetColor( g_color_table[currentColor] );

	v = 0;

#ifdef FX3
	for(i=con[CON_ALL].current - con_notifylines->integer; i <= con[CON_ALL].current ; i++){
		if (i < 0)
			continue;
		time = con[CON_ALL].times[i % NUM_CON_TIMES];
		if (time == 0){
			continue;
		}
		time = cls.realtime - time;
		if (time > con_notifytime->value*1000){
			continue;
		}
		text = con[CON_ALL].text + (i % con[CON_ALL].totallines)*con[CON_ALL].linewidth;

		if (cl.snap.ps.pm_type != PM_INTERMISSION && Key_GetCatcher( ) & (KEYCATCH_UI | KEYCATCH_CGAME) ){
			continue;
		}

		for (x = 0 ; x < con[CON_ALL].linewidth ; x++) {
			if ( ( text[x] & 0xff ) == ' ' ) {
				continue;
			}
			if ( ( (text[x]>>8) % NUMBER_OF_COLORS ) != currentColor ) {
				currentColor = (text[x]>>8) % NUMBER_OF_COLORS;
				re.SetColor( g_color_table[currentColor] );
			}
			SCR_DrawSmallChar( cl_conXOffset->integer + con[CON_ALL].xadjust + (x+1)*SMALLCHAR_WIDTH, cl_conYOffset->integer + v, text[x] & 0xff );
		}
#else
	for (i= con.current-NUM_CON_TIMES+1 ; i<=con.current ; i++)
	{
		if (i < 0)
			continue;
		time = con.times[i % NUM_CON_TIMES];
		if (time == 0)
			continue;
		time = cls.realtime - time;
		if (time > con_notifytime->value*1000)
			continue;
		text = con.text + (i % con.totallines)*con.linewidth;

		if (cl.snap.ps.pm_type != PM_INTERMISSION && Key_GetCatcher( ) & (KEYCATCH_UI | KEYCATCH_CGAME) ) {
			continue;
		}

		for (x = 0 ; x < con.linewidth ; x++) {
			if ( ( text[x] & 0xff ) == ' ' ) {
				continue;
			}
			if ( ColorIndexForNumber( text[x]>>8 ) != currentColor ) {
				currentColor = ColorIndexForNumber( text[x]>>8 );
				re.SetColor( g_color_table[currentColor] );
			}
			SCR_DrawSmallChar( cl_conXOffset->integer + con.xadjust + (x+1)*SMALLCHAR_WIDTH, v, text[x] & 0xff );
		}
#endif

		v += SMALLCHAR_HEIGHT;
	}

	re.SetColor( NULL );

	if (Key_GetCatcher( ) & (KEYCATCH_UI | KEYCATCH_CGAME) ) {
		return;
	}

	// draw the chat line
	if ( Key_GetCatcher( ) & KEYCATCH_MESSAGE )
	{
		if (chat_team)
		{
			SCR_DrawBigString (8, v, "say_team:", 1.0f, qfalse );
			skip = 10;
		}
		else
		{
			SCR_DrawBigString (8, v, "say:", 1.0f, qfalse );
			skip = 5;
		}

		Field_BigDraw( &chatField, skip * BIGCHAR_WIDTH, v,
			SCREEN_WIDTH - ( skip + 1 ) * BIGCHAR_WIDTH, qtrue, qtrue );
	}

}

/*
================
Con_DrawSolidConsole

Draws the console with the solid background
================
*/
void Con_DrawSolidConsole( float frac ) {
	int				i, x, y;
	int				rows;
	short			*text;
	int				row;
	int				lines;
//	qhandle_t		conShader;
	int				currentColor;
	vec4_t			color;
#ifdef FX3
	int			j;
	qtime_t			qt;
	char			*curLTime;
	char			*ampm;
	int			ampm_hour;

	//  for the console tabs
	int tabWidth, tabHeight, horizOffset, vertOffset, margin;
	vec4_t lineColor, darkTextColor;
#endif


	lines = cls.glconfig.vidHeight * frac;
	if (lines <= 0)
		return;

	if (lines > cls.glconfig.vidHeight )
		lines = cls.glconfig.vidHeight;

	// on wide screens, we will center the text
#ifdef FX3
	activeCon->xadjust = 0;
	SCR_AdjustFrom640( &activeCon->xadjust, NULL, NULL, NULL );
#else
	con.xadjust = 0;
	SCR_AdjustFrom640( &con.xadjust, NULL, NULL, NULL );
#endif

	// draw the background
	y = frac * SCREEN_HEIGHT;
	if ( y < 1 ) {
		y = 0;
	}
	else {
#ifdef FX3
//  like the console from OA
		if ( cl_consoleType->integer ) {
			color[0] = cl_consoleType->integer > 1 ? cl_consoleColor[0]->value : 1.0f ;
			color[1] = cl_consoleType->integer > 1 ? cl_consoleColor[1]->value : 1.0f ;
			color[2] = cl_consoleType->integer > 1 ? cl_consoleColor[2]->value : 1.0f ;
			color[3] = cl_consoleColor[3]->value;
			re.SetColor( color );
		}
		if ( cl_consoleType->integer == 2 ) {
			SCR_DrawPic( 0, 0, SCREEN_WIDTH, y, cls.whiteShader );
		} else {
			SCR_DrawPic( 0, 0, SCREEN_WIDTH, y, cls.consoleShader );
		}
#else
		SCR_DrawPic( 0, 0, SCREEN_WIDTH, y, cls.consoleShader );
#endif
	}

	color[0] = 1;
	color[1] = 0;
	color[2] = 0;
#ifdef FX3
	if( !cl_consoleType->integer )
#endif
	color[3] = 1;
#ifdef FX3
	SCR_FillRect( 0, y, SCREEN_WIDTH, 2, g_color_table[4] );
#else
	SCR_FillRect( 0, y, SCREEN_WIDTH, 2, color );
#endif


	// draw the version number

#ifdef FX3
	re.SetColor( g_color_table[ColorIndex(COLOR_BLUE)] );
#else
	re.SetColor( g_color_table[ColorIndex(COLOR_RED)] );
#endif

	i = strlen( Q3_VERSION );

	for (x=0 ; x<i ; x++) {
		SCR_DrawSmallChar( cls.glconfig.vidWidth - ( i - x + 1 ) * SMALLCHAR_WIDTH,
#ifdef FX3
			lines - SMALLCHAR_HEIGHT + 1, Q3_VERSION[x] );
#else
			lines - SMALLCHAR_HEIGHT, Q3_VERSION[x] );
#endif
	}

#ifdef FX3
	//  draw local time on console
	Com_RealTime(&qt);

	if(Cvar_VariableIntegerValue("con_clocktype") == 0){
		curLTime = va ("%02i:%02i:%02i", qt.tm_hour, qt.tm_min, qt.tm_sec );
	} else {
		if(qt.tm_hour < 13){
			ampm = "AM";
			ampm_hour = qt.tm_hour;
		} else {
			ampm = "PM";
			ampm_hour = qt.tm_hour - 12;
		}
		curLTime = va ("%02i:%02i%s", ampm_hour, qt.tm_min, ampm );
	}

	i = strlen(curLTime);

	SCR_DrawSmallStringExt( cls.glconfig.vidWidth - i * SMALLCHAR_WIDTH, 1, curLTime, g_color_table[5], qfalse, qtrue);

	// this is mostly inspired by an ioq3-UrT client, can't remember which one
	margin = 3;
        horizOffset = margin;
        vertOffset = lines - SMALLCHAR_HEIGHT;

        lineColor[0] = lineColor[1] = lineColor[2] = 0.6;

        darkTextColor[0] = darkTextColor[1] = darkTextColor[2] = 0.25;
        darkTextColor[3] = 1;

	for (j = 0; j < MAX_CONSOLES; j++){
          if(con[j].active == qtrue){
                if (activeCon == &con[j]) {
                        tabWidth = strlen(con[j].name) * SMALLCHAR_WIDTH + 4;
                        tabHeight = SMALLCHAR_HEIGHT + 2;
                        lineColor[3] = 1;
                } else {
                        tabWidth = strlen(con[j].name) * SMALLCHAR_WIDTH + 2;
                        tabHeight = SMALLCHAR_HEIGHT + 1;
                        lineColor[3] = 0.3;
                }

		//tab background
                SCR_DrawRect(horizOffset, vertOffset, tabWidth, tabHeight, lineColor);

                // bottom border
                SCR_DrawRect(horizOffset, vertOffset + tabHeight - 1, tabWidth, 1, lineColor);

                // left border
                if ((!j && margin) || (j && activeCon == &con[j])) {
                        SCR_DrawRect(horizOffset, vertOffset, 1, tabHeight, lineColor);
                }

		// right border
                SCR_DrawRect(horizOffset + tabWidth, vertOffset, 1, tabHeight, lineColor);
                if (activeCon == &con[j]) {
                        SCR_DrawSmallStringExt(horizOffset + 1, vertOffset + 2, con[j].name, g_color_table[4], qfalse, qtrue);

                } else {
                        if(con[j].notify == qtrue){
                                SCR_DrawSmallStringExt(horizOffset + 1, vertOffset + 1, con[j].name, g_color_table[3], qfalse, qtrue);
                        } else {
                                SCR_DrawSmallStringExt(horizOffset + 1, vertOffset + 1, con[j].name, darkTextColor, qfalse, qtrue);
                        }
                }

                horizOffset += tabWidth;
          }
        }

	//  just draw a white line above the tabs
        SCR_DrawRect(0, vertOffset, 1920, 1, g_color_table[5]);
#endif

	

	// draw the text
#ifdef FX3
	activeCon->vislines = lines;
#else
	con.vislines = lines;
#endif
	rows = (lines-SMALLCHAR_HEIGHT)/SMALLCHAR_HEIGHT;		// rows of text to draw

	y = lines - (SMALLCHAR_HEIGHT*3);

	// draw from the bottom up
#ifdef FX3
	if (activeCon->display != activeCon->current)
#else
	if (con.display != con.current)
#endif
	{
	// draw arrows to show the buffer is backscrolled
		re.SetColor( g_color_table[ColorIndex(COLOR_RED)] );
#ifdef FX3
		for (x=0 ; x < activeCon->linewidth ; x+=4)
			SCR_DrawSmallChar( activeCon->xadjust + (x+1)*SMALLCHAR_WIDTH, y, '^' );
#else
		for (x=0 ; x<con.linewidth ; x+=4)
			SCR_DrawSmallChar( con.xadjust + (x+1)*SMALLCHAR_WIDTH, y, '^' );
#endif
		y -= SMALLCHAR_HEIGHT;
		rows--;
	}

#ifdef FX3
	row = activeCon->display;

	if ( activeCon->x == 0 ) {
#else	
	row = con.display;

	if ( con.x == 0 ) {
#endif
		row--;
	}

	currentColor = 7;
	re.SetColor( g_color_table[currentColor] );

	for (i=0 ; i<rows ; i++, y -= SMALLCHAR_HEIGHT, row--)
	{
		if (row < 0)
			break;
#ifdef FX3
		if (activeCon->current - row >= activeCon->totallines) {
			// past scrollback wrap point
			continue;
		}

		text = activeCon->text + (row % activeCon->totallines)*activeCon->linewidth;

		for (x=0 ; x < activeCon->linewidth ; x++) {
			if ( ( text[x] & 0xff ) == ' ' ) {
				continue;
			}

			if ( ( (text[x]>>8) % NUMBER_OF_COLORS ) != currentColor ) {
				currentColor = (text[x]>>8) % NUMBER_OF_COLORS;
				re.SetColor( g_color_table[currentColor] );
			}
			SCR_DrawSmallChar(  activeCon->xadjust + (x+1)*SMALLCHAR_WIDTH, y, text[x] & 0xff );
#else
		if (con.current - row >= con.totallines) {
			// past scrollback wrap point
			continue;	
		}

		text = con.text + (row % con.totallines)*con.linewidth;

		for (x=0 ; x<con.linewidth ; x++) {
			if ( ( text[x] & 0xff ) == ' ' ) {
				continue;
			}

			if ( ColorIndexForNumber( text[x]>>8 ) != currentColor ) {
				currentColor = ColorIndexForNumber( text[x]>>8 );
				re.SetColor( g_color_table[currentColor] );
			}
			SCR_DrawSmallChar(  con.xadjust + (x+1)*SMALLCHAR_WIDTH, y, text[x] & 0xff );
#endif
		}
	}

	// draw the input prompt, user text, and cursor if desired
	Con_DrawInput ();

	re.SetColor( NULL );
}

#ifdef FX3
//  this is a function to extract values separated by whitespaces from a string
void CL_ExtractToken(char **ptr, char *token){
        while(**ptr == '"' || **ptr == ' ' || **ptr == '\0'){
                if(**ptr == '\0')
                        return;

                (*ptr)++;
        }

        while(**ptr != '"' && **ptr != ' ' && **ptr != '\0'){
                *token = **ptr;
                (*ptr)++;
                token++;
        }

        *token = '\0';
}
#endif

/*
==================
Con_DrawConsole
==================
*/
void Con_DrawConsole( void ) {
	// check for console width changes from a vid mode change
#ifdef FX3
	Con_CheckResize (activeCon);
#else
	Con_CheckResize ();
#endif

	// if disconnected, render console full screen
	if ( clc.state == CA_DISCONNECTED ) {
		if ( !( Key_GetCatcher( ) & (KEYCATCH_UI | KEYCATCH_CGAME)) ) {
			Con_DrawSolidConsole( 1.0 );
			return;
		}
	}

#ifdef FX3
	if ( activeCon->displayFrac ) {
		Con_DrawSolidConsole( activeCon->displayFrac );
#else
	if ( con.displayFrac ) {
		Con_DrawSolidConsole( con.displayFrac );
#endif
	} else {
		// draw notify lines
		if ( clc.state == CA_ACTIVE ) {
			Con_DrawNotify ();
		}
	}
}

//================================================================

/*
==================
Con_RunConsole

Scroll it up or down
==================
*/
void Con_RunConsole (void) {
#ifdef FX3
	int i;
#endif

#ifdef CONFILTER
	// Cgg - check for updated con_filters
	cvar_t **cvar;
	pcre **re;
	const char *errptr;
	int erroffset;

	for (cvar=con_filters; cvar-con_filters<MAX_CON_FILTERS; cvar++) {
		if (!*cvar || !(*cvar)->modified)
			continue;

		re = con_filters_compiled+(cvar-con_filters);
		(*cvar)->modified = qfalse;

		if (!strlen((*cvar)->string)) {
			*re = NULL;
			continue;
		}

		*re = pcre_compile((*cvar)->string, 0, &errptr, &erroffset, NULL);

		if (!*re) {
			Com_Printf("Failed to compile %c%s\n", Q_RAW_ESCAPE, (*cvar)->string);
			Com_Printf(va("%c%%%ic %%s\n", Q_RAW_ESCAPE, erroffset+19), '^', errptr);
			Cvar_Set((*cvar)->name, "");
			(*cvar)->modified = qfalse;
		}
	}
	// !Cgg
#endif

#ifdef FX3
	for(i = 0; i < MAX_CONSOLES; i++){
		// decide on the destination height of the console
		if ( Key_GetCatcher( ) & KEYCATCH_CONSOLE ){
			con[i].finalFrac = con[i].userFrac;
		} else {
			con[i].finalFrac = 0;
		}

		// scroll towards the destination height
		if (con[i].finalFrac < con[i].displayFrac){
			con[i].displayFrac -= con_conspeed->value*cls.realFrametime*0.001;
			if (con[i].finalFrac > con[i].displayFrac){
				con[i].displayFrac = con[i].finalFrac;
			}
		} else if (con[i].finalFrac > con[i].displayFrac){
			con[i].displayFrac += con_conspeed->value*cls.realFrametime*0.001;
			if (con[i].finalFrac < con[i].displayFrac){
				con[i].displayFrac = con[i].finalFrac;
			}
		}
	}
#else
	// decide on the destination height of the console
	if ( Key_GetCatcher( ) & KEYCATCH_CONSOLE )
		con.finalFrac = 0.5;		// half screen
	else
		con.finalFrac = 0;				// none visible
	
	// scroll towards the destination height
	if (con.finalFrac < con.displayFrac)
	{
		con.displayFrac -= con_conspeed->value*cls.realFrametime*0.001;
		if (con.finalFrac > con.displayFrac)
			con.displayFrac = con.finalFrac;

	}
	else if (con.finalFrac > con.displayFrac)
	{
		con.displayFrac += con_conspeed->value*cls.realFrametime*0.001;
		if (con.finalFrac < con.displayFrac)
			con.displayFrac = con.finalFrac;
	}
#endif
}

#ifdef FX3
/*
==================
Con_SetFrac
==================
*/
void Con_SetFrac(const float conFrac) {
	int i;

	for(i = 0; i < MAX_CONSOLES; i++){
		// clamp the cvar value
		if (conFrac < .1f) {    // don't let the console be hidden
			con[i].userFrac = .1f;
		} else if (conFrac > 1.0f) {
			con[i].userFrac = 1.0f;
		} else {
			con[i].userFrac = conFrac;
		}
	}
}

void Con_PageUp( void ) {
	activeCon->display -= 2;
	if ( activeCon->current - activeCon->display >= activeCon->totallines ) {
		activeCon->display = activeCon->current - activeCon->totallines + 1;
	}
}

void Con_PageDown( void ) {
	activeCon->display += 2;
	if (activeCon->display > activeCon->current) {
		activeCon->display = activeCon->current;
	}
}

void Con_Top( void ) {
	activeCon->display = activeCon->totallines;
	if ( activeCon->current - activeCon->display >= activeCon->totallines ) {
		activeCon->display = activeCon->current - activeCon->totallines + 1;
	}
}

void Con_Bottom( void ) {
	activeCon->display = activeCon->current;
}
#else

void Con_PageUp( void ) {
	con.display -= 2;
	if ( con.current - con.display >= con.totallines ) {
		con.display = con.current - con.totallines + 1;
	}
}

void Con_PageDown( void ) {
	con.display += 2;
	if (con.display > con.current) {
		con.display = con.current;
	}
}

void Con_Top( void ) {
	con.display = con.totallines;
	if ( con.current - con.display >= con.totallines ) {
		con.display = con.current - con.totallines + 1;
	}
}

void Con_Bottom( void ) {
	con.display = con.current;
}
#endif

void Con_Close( void ) {
	if ( !com_cl_running->integer ) {
		return;
	}
	Field_Clear( &g_consoleField );
	Con_ClearNotify ();
	Key_SetCatcher( Key_GetCatcher( ) & ~KEYCATCH_CONSOLE );
#ifdef FX3
	activeCon->finalFrac = 0;
	activeCon->displayFrac = 0;
#else
	con.finalFrac = 0;				// none visible
	con.displayFrac = 0;
#endif
}

#ifdef FX3
int Con_GetNum(void) {
	return activeConsoleNum;
}
#endif
